# 📊 Optimized Data Pipeline for Scalable Analytics with Snowflake and DevOps Practices 🚀

## 🌟 Overview
This project demonstrates an end-to-end data pipeline that leverages Snowflake for data warehousing, combined with robust DevOps practices including CI/CD, automated testing, and infrastructure management using Terraform. The purpose of this project is to showcase an automated, scalable approach to managing data workflows in a cloud environment.

## ✨ Features
- **📥 Data Ingestion**: Stream data from Kafka into Snowflake.
- **🔄 Data Transformation**: Perform SQL-based transformations within Snowflake to prepare data for analytics.
- **🧹 Data Cleaning**: Scripts to ensure data quality and consistency.
- **🔍 Testing**: Automated Python scripts to validate data integrity and query performance.
- **🛠️ CI/CD Pipeline**: Using GitLab CI/CD to automate the testing and deployment phases.
- **🏗️ Infrastructure as Code**: Manage Snowflake resources using Terraform.

## 📋 Prerequisites
- Snowflake account
- Kafka setup (for data ingestion)
- Python 3.8 or higher
- Terraform
- GitLab account (for CI/CD pipeline)
- Access to `snowsql` CLI tool

## 📄 License
This project is licensed under the MIT License - see the LICENSE.md file for details.
