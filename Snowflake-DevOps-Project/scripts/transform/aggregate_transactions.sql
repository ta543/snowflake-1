-- aggregate_transactions.sql
CREATE OR REPLACE TABLE aggregated_transactions AS
SELECT
    DATE(timestamp) as transaction_date,
    COUNT(*) as total_transactions,
    SUM(amount) as total_amount,
    AVG(amount) as average_transaction_amount
FROM
    transaction_records
GROUP BY
    DATE(timestamp);
