# kafka_to_snowflake.py
import snowflake.connector
from kafka import KafkaConsumer
import json

# Kafka consumer setup
consumer = KafkaConsumer(
    'transaction_data',
    bootstrap_servers=['kafka.example.com:9092'],
    value_deserializer=lambda m: json.loads(m.decode('utf-8'))
)

# Snowflake connection setup
conn = snowflake.connector.connect(
    user='finance_admin',
    password='securePassword123!',
    account='xy12345.us-east-1',
    warehouse='financials_compute',
    database='financial_records',
    schema='raw_data'
)
cursor = conn.cursor()

# Function to insert data into Snowflake
def insert_data(data):
    query = """
    INSERT INTO transaction_records(transaction_id, amount, timestamp)
    VALUES (%s, %s, %s)
    """
    cursor.execute(query, (data['transaction_id'], data['amount'], data['timestamp']))

# Consuming messages from Kafka
for message in consumer:
    insert_data(message.value)

# Close the database connection
cursor.close()
conn.close()
