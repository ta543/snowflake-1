-- remove_duplicates.sql
DELETE FROM transaction_records
WHERE record_id IN (
    SELECT record_id
    FROM (
        SELECT record_id,
               ROW_NUMBER() OVER (PARTITION BY transaction_id ORDER BY transaction_time DESC) as rn
        FROM transaction_records
    ) sub
    WHERE sub.rn > 1
);
