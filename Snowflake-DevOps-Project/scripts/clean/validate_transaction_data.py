# validate_transaction_data.py
import snowflake.connector

# Establish connection
conn = snowflake.connector.connect(
    user='finance_admin',
    password='securePassword123!',
    account='xy12345.us-east-1',
    warehouse='financials_compute',
    database='financial_records',
    schema='raw_data'
)
cursor = conn.cursor()

# Validate data
validation_query = """
SELECT COUNT(*)
FROM transaction_records
WHERE amount <= 0 OR transaction_id IS NULL
"""

cursor.execute(validation_query)
result = cursor.fetchone()
if result[0] > 0:
    print(f"Data validation failed: {result[0]} invalid records found.")
else:
    print("Data validation passed: All records are valid.")

# Close connection
cursor.close()
conn.close()
