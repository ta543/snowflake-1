variable "snowflake_username" {
  type        = string
  description = "Username for the Snowflake account."
  default     = "user"
}

variable "snowflake_password" {
  type        = string
  description = "Password for the Snowflake account."
  sensitive   = true
}

variable "snowflake_account" {
  type        = string
  description = "Account identifier for Snowflake."
  default     = "xy12345"
}

variable "region" {
  type        = string
  default     = "us-west-2"
  description = "Region where the Snowflake account is hosted."
}
