output "warehouse_name" {
  value       = snowflake_warehouse.main.name
  description = "The name of the created Snowflake warehouse."
}

output "database_name" {
  value       = snowflake_database.main.name
  description = "The name of the created Snowflake database."
}

output "devops_role_name" {
  value       = snowflake_role.devops_role.name
  description = "The name of the created Snowflake role for DevOps."
}
