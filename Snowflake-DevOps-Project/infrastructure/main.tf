terraform {
  required_providers {
    snowflake = {
      source = "chanzuckerberg/snowflake"
      version = "0.25.20"
    }
  }

  required_version = ">= 0.14"
}

provider "snowflake" {
  username      = var.snowflake_username
  password      = var.snowflake_password
  account       = var.snowflake_account
  role          = "ACCOUNTADMIN"
  region        = var.region
}

resource "snowflake_warehouse" "main" {
  name                         = "MAIN_WAREHOUSE"
  warehouse_size               = "X-SMALL"
  auto_suspend                 = 120
  auto_resume                  = true
  initially_suspended          = true
}

resource "snowflake_database" "main" {
  name = "MAIN_DATABASE"
}

resource "snowflake_role" "devops_role" {
  name = "DEVOPS_ROLE"
}

resource "snowflake_role_grants" "devops_role_grants" {
  role_name = snowflake_role.devops_role.name

  roles = [
    "SYSADMIN"
  ]

  privileges = [
    "MONITOR"
  ]
}
