# data_validation_tests.py
import snowflake.connector
import unittest

class TestDataValidation(unittest.TestCase):
    def setUp(self):
        # Connect to Snowflake
        self.conn = snowflake.connector.connect(
            user='finance_admin',
            password='securePassword123!',
            account='xy12345.us-east-1',
            warehouse='financials_compute',
            database='financial_records',
            schema='raw_data'
        )
        self.cursor = self.conn.cursor()

    def test_null_values(self):
        # Check for null values in critical columns
        query = "SELECT COUNT(*) FROM transaction_records WHERE transaction_id IS NULL OR amount IS NULL"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.assertEqual(result[0], 0, "Null values found in critical columns")

    def test_negative_values(self):
        # Check for negative values in the amount field
        query = "SELECT COUNT(*) FROM transaction_records WHERE amount < 0"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.assertEqual(result[0], 0, "Negative values found in amount column")

    def tearDown(self):
        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    unittest.main()
